# CamelCaseify

An AngularJS application generator.

The generator initializes a fully functional AngularJS application including all necessary frontend and dev dependencies as well es running unit and acceptance tests.

# Installation

### node / npm

Installs the node JavaScript platform as well as the node package mangager.

    https://gist.github.com/isaacs/579814

* The 30 seconds guide did suffice on Ubuntu 13.10

### grunt-cli and grunt-init
 
Installs the JavaScript task runner.
   
    npm install grunt-cli -g;
    npm install grunt-init -g

* grunt-cli also installs grunt as a dependency
* We need both globally available on the system

### bower

Installs the frontend module package manager.

    npm install bower -g;

* We need to have bower installed globally.

# Setup application

1. Checkout this generator template

        cd ~/projects/grunt-init-templates
        cd ~/.grunt-init

2. Change into an arbitrary folder where you want to install the new app.

        cd ~/projects/my-js-apps

 * The generator creates a subdirectory for each generated app.

3. run grunt-init [template path]
    
        ~/projects/my-js-apps$ grunt-init ../path/to/camelcaseify

4. Answer the questions of the installer (name and description)

5. Change into the genrated application.
        
        cd camelcaseify

6. Install all dev and production dependencies

        npm install && bower install

# Run the application

### Run the unit tests
    $ grunt test

### Run the acceptance test
    $ grunt e2e

### Run jshint and all tests
    $ grunt

### Run the embedded server to test locally
    $ grunt server

   * And point your browser to [http://localhost:9181](http://localhost:9181)