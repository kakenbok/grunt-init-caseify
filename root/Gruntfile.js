'use strict';

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    var config = {
        app: 'app',
        build: 'build',
        dist: 'build/dist',
        test: 'test'
    };

    grunt.initConfig({
        config: config,
        connect: {
            options: {
                port: 9181,
                hostname: 'localhost'
            },
            server: {
                options: {
                    base: [
                        '<%= config.app %>'
                    ]
                }
            },
            test: {
                options: {
                    port: 9182,
                    base: [
                        '<%= config.test %>',
                        '<%= config.app %>'
                    ]
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                '<%= config.app %>/js/**/*.js',
                '<%= config.test %>/**/*.js'
            ]
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            },
            e2e: {
                configFile: 'karma-e2e.conf.js',
                singleRun: true
            }
        }
    });

    grunt.registerTask('server', function () {
        return grunt.task.run(['connect:server:keepalive']);
    });
    
    grunt.registerTask('test', [
        'jshint',
        'karma:unit'
    ]);
    
    grunt.registerTask('e2e', [
        'connect:test',
        'karma:e2e'
    ]);
    
    grunt.registerTask('default', [
        'test',
        'e2e'
    ]);
  
};

