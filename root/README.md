# {%= name %}

# Run the application

### Run the unit tests
    ~/Projects/../{%= appDir %}$ grunt test

### Run the acceptance test
    ~/Projects/../{%= appDir %}$ grunt e2e

### Run jshint and all tests
    ~/Projects/../{%= appDir %}$ grunt

### Run the embedded server to test locally
    $ grunt server

    * And point your browser to [http://localhost:9181/#/{%= appDir %}](http://localhost:9181/#/{%= appDir %})
   
