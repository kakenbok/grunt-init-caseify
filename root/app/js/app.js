'use strict';

angular.module('{%= appName %}', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ui.bootstrap'
])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/main.html',
                controller: 'MainCtrl'
            })
            .when('/{%= appDir %}', {
                templateUrl: 'partials/main.html',
                controller: 'MainCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
