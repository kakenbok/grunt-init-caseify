'use strict';

angular.module('{%= appName %}')
    .controller('MainCtrl', ['$scope', function($scope) {
        $scope.submit = function(textInput) {
            $scope.showAlert(textInput);
        };

        $scope.showAlert = function(message) {
            $scope.hideAlert();
            $scope.alerts.push({
                type: 'success',
                msg: message
            });
        };

        $scope.hideAlert = function() {
            $scope.alerts = [];
        };
    }]);
