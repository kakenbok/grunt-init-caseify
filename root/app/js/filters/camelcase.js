'use strict';

angular.module('{%= appName %}')
    .filter('camelCase', function() {
        return function(text) {
            if (!text) {
                return '';
            }
            text = text.replace(/([\s\-])+/g, '$1');
            text = text.replace(/^[\s\-]+|[\s\-]+$/g, '');
            return text.replace(/(?:^|[- ])(.)/g, function(match, p1) {
                if (p1 === ' ' || p1 === '-') {
                    return '';
                }
                return p1.toUpperCase();
            });
        };
    });