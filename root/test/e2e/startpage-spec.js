'use strict';

describe('Startpage Tests', function() {

    beforeEach(function() {
        browser().navigateTo('/#/{%= appDir %}');
    });
    
    it('redirect works', function() {
        expect(browser().location().url()).toBe('/{%= appDir %}');
    });

    it('button disabled when input field empty', function() {
        expect(element('.btn').attr('disabled')).toBe('disabled');
    });

    it('button disabled when input field filled with empty text', function() {
        inputText('                   ');
        expect(element('.btn').attr('disabled')).toBe('disabled');
    });

/*
    edge case, to be fixed!
    
    it('button disabled when input field filled with empty text and dashes', function() {
        inputText('           --- --   ---    ');
        expect(element('.btn').attr('disabled')).toBe('disabled');
    });
*/

    it('button enabled when input field filled', function() {
        inputText('this is yet a cool thing');
        expect(element('.btn').attr('disabled')).not().toBeDefined('');
    });

    it('button again disabled when input field emptied', function() {
        inputText('this is yet a cool thing');
        expect(element('.btn').attr('disabled')).not().toBeDefined('');
        inputText('');
        expect(element('.btn').attr('disabled')).toBe('disabled');
    });

    it('button click raises alert', function() {
        inputTextAndClickButton('this is really nice');
    });
    
    it('alert shows right message', function() {
        inputTextAndClickButton('this is really nice');
        
        expect(element('.alert').text()).toContain('Yeah! Your submission "ThisIsReallyNice" has been approved!');
    });

    it('alert can be closed', function() {
        inputTextAndClickButton('this is really nice');
        
        element('.alert button').click();

        expect(element('.alert')).not().toBeDefined();
    });

    function inputText(text) {
        element('#textInput').val('');

        var inputElm = input('textInput');
        
        expect(inputElm.val()).toBe('');
        inputElm.enter(text);
        expect(inputElm.val()).toBe(text);
    }

    function inputTextAndClickButton(text) {
        inputText(text);

        expect(element('.alert')).not().toBeDefined();
        
        element('.btn').click();
        
        expect(element('.alert')).not().toBeNull();
    }

});