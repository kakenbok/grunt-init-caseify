'use strict';

describe('camelCase', function() {

    beforeEach(module('{%= appName %}'));
    
    var camelCase;
    
    beforeEach(inject(function ($filter) {
        camelCase = $filter('camelCase');
    }));

    describe('camelCase', function() {

        it('should handle the common case', function() {
            expect(camelCase('should handle the common case')).toBe('ShouldHandleTheCommonCase');
        });

        it('should handle punctuation', function() {
            expect(camelCase('eingabe, mit: Leerzeichen und PUNKT.')).toBe('Eingabe,Mit:LeerzeichenUndPUNKT.');
        });

        it('should preserve explicit uppercase', function() {
            expect(camelCase('This IsMeant To stay UPPER'))
                .toBe('ThisIsMeantToStayUPPER');
        });

        it('should handle numbers', function() {
            expect(camelCase('there are 1 12 or 123 people'))
                .toBe('ThereAre112Or123People');
        });

        it('should treat dashes like spaces', function() {
            expect(camelCase('there-are-really-many-people'))
                .toBe('ThereAreReallyManyPeople');
        });

        it('should trim dashes on boundaries', function() {
            expect(camelCase('-this is not so good-'))
                .toBe('ThisIsNotSoGood');
        });

        it('should trim spaces on boundaries', function() {
            expect(camelCase(' this is not so good '))
                .toBe('ThisIsNotSoGood');
        });

        it('should remove multiple spaces', function() {
            expect(camelCase('  there   are  lot   of spaces  '))
                .toBe('ThereAreLotOfSpaces');
        });

        it('should remove multiple dashes', function() {
            expect(camelCase('--there--are---lot---of-dashes-as-well--'))
                .toBe('ThereAreLotOfDashesAsWell');
        });

        it('should make empty if only dash or space', function() {
            expect(camelCase('--   --   ---   ---  -   '))
                .toBe('');
        });

    });
});