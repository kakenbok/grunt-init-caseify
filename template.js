"use strict";

exports.notes = "Create the caseify app layout based on AngularJS, bootstrap, jshint and karma testing).";

exports.template = function(grunt, init, done) {

    init.process({type: "jquery"}, [
        {
            name: "name",
            message: "What is the name of the new app?",
            default: "CamelCaseify",
            validator: /^[a-zA-Z][\w\-]+\w$/,
            warning: "Must be only word characters or dash (/^[a-zA-Z][\w\-]+\w$/)."

        },
        init.prompt("description", "The smart camel case builder tool.")

    ], function(err, props) {
        props.appDir = props.name.replace(/([a-z])([A-Z])/, "$1-$2").toLowerCase();
        props.appName = props.name.replace(/(?:^|[- ])(.)/g, function(match, p1) {
            if (p1 === ' ' || p1 === '-') {
                return;
            }
            return p1.toUpperCase();
        });

        var join = require("path").join;
        grunt.file.mkdir(join(init.destpath(), props.appDir));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/app/img"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/app/lib"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/app/js/directives"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/app/js/services"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/app/js/factories"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/test/unit/directives"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/test/unit/services"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/test/unit/factories"));
        grunt.file.mkdir(join(init.destpath(), props.appDir + "/test/unit/controllers"));

        var genericFiles = init.filesToCopy(props);
        var files = {};
        for (var genericDest in genericFiles) {
            var dest = props.appDir + '/' + genericDest;
            files[dest] = genericFiles[genericDest];
        }

        init.copyAndProcess(files, props);

        init.writePackageJSON(props.appDir + "/package.json", {
            name: props.name,
            description: props.description,
            version: "0.1.0",
            devDependencies: {
                "grunt": "~0.4.1",
                "load-grunt-tasks": "~0.2.0",
                "grunt-contrib-connect": "~0.5.0",
                "grunt-contrib-jshint": "~0.7.1",
                "grunt-karma": "~0.6.2",
                "karma": "~0.10.4",
                "karma-ng-scenario": "~0.1.0"
            },
            engine: "node 0.10.x"
        });

        init.writePackageJSON(props.appDir + "/bower.json", {
            name: props.name,
            dependencies: {
                "bootstrap": "3.x.x",
                "angular": "1.x.x",
                "angular-cookies": "1.x.x",
                "angular-resource": "1.x.x",
                "angular-route": "1.x.x",
                "angular-bootstrap": "0.x.x",
                "jquery": "2.x.x"
            },
            devDependencies: {
                "angular-mocks": "1.x.x"                
            }
        });

        done();
    });

};